# API Contact Management

Pet project for training Mobile API Developer

1. Create file `local_root.py`, copy content from `local_root.py.yml`.
2. Each models must be inherited `from core.models import BaseModel`, each module create a folder in `core` for easy manage.
3. API views write in `api` folder. Each module representing a folder in `api`
4. Create local database (highly recommended PosgreSQL or MySQL). Using command `python manage.py makemigrations` and `python manage.py migrate`.
5. Each API must be write wiki. Sample : `https://git.minerva.vn/datpt/api-contact-management/wikis/sample`.
6. Analyst contact.google.com for design database.

* Using simple Json web token `https://github.com/davesque/django-rest-framework-simplejwt`.