from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, db_column='created_at', blank=True, null=True, verbose_name=_('Created at'))
    updated_at = models.DateTimeField(auto_now=True, db_column='modified_at', blank=True, null=True, verbose_name=_('Updated at'))
    created_by = models.CharField(max_length=100, db_column='created_by', blank=True, null=True, default='', verbose_name=_('Created by'))
    updated_by = models.CharField(max_length=100, db_column='modified_by', blank=True, null=True, default='', verbose_name=_('Updated by'))

    class Meta:
        abstract = True

    def get_required_fields(self):
        fields = self._meta.get_fields()
        required_fields = []

        # Required means `blank` is False
        for f in fields:
            # Note - if the field doesn't have a `blank` attribute it is probably
            # a ManyToOne relation (reverse foreign key), which you probably want to ignore.
            if hasattr(f, 'blank') and f.blank is False:
                required_fields.append(f)
        return required_fields

    def get_required_fields_as_names(self):
        str_required_field_names = []
        required_fields = self.get_required_fields()
        for required_field in required_fields:
            str_required_field_names.append(required_field.name)
        return str_required_field_names

class Contact(BaseModel):
    id = models.AutoField(primary_key=True)
    phonenumber = models.CharField(max_length=11, db_column='phone_number', blank=False, verbose_name=_('Phone number'))
    name = models.CharField(max_length=500, db_column='name', blank=False, verbose_name=_('Name'))
    verify = models.BooleanField(db_column='verify', blank=True, default=False, verbose_name=_('Verify'))

    def __str__(self):
        return self.phonenumber

    class Meta:
        ordering = ["-created_at", "-updated_at"]

class Tag(BaseModel):
    id = models.AutoField(primary_key=True)
    tagname = models.CharField(max_length=11, db_column='tag_name', blank=False, verbose_name=_('Tag name'), unique=True)
    contacts = models.ManyToManyField(Contact, blank=True)

    def __str__(self):
        return self.tagname

    class Meta:
        ordering = ["-created_at", "-updated_at"]