from rest_framework import HTTP_HEADER_ENCODING
from django.utils.six import text_type
import json
import zlib


CONTENT_TYPE_JSON = b'application/json'
CONTENT_TYPE_FROM_DATA = b'multipart/form-data'
CONTENT_TYPE_IMAGE = b'image/png'
CONTENT_TYPE_TEXT = b'text/plain'
CONTENT_TYPE_VIDEO = b'audio/mp4'

IP_SOCKET_LOCAL = '127.0.0.1'

HTTP_MINERVA = ('Mnv#!535', )

KEY_PASS_RESET = 'c9ad-01b9-4d66-bc88-06b4-49aj'


def get_ios_authorization_header(request):
    """
    Return request's 'Authorization:' header, as a bytestring.

    Hide some test client ickyness where the header can be unicode.
    """
    auth = request.META.get('HTTP_MINERVA', b'')

    if isinstance(auth, text_type):
        # Work around django test client oddness
        auth = auth.encode(HTTP_HEADER_ENCODING)
    return auth


def get_content_type_header(request):
    """
    Return request's 'Authorization:' header, as a bytestring.

    Hide some test client ickyness where the header can be unicode.
    """
    content = request.META.get('CONTENT_TYPE', b'')

    try:
        content = content.encode(HTTP_HEADER_ENCODING)
    except:
        content = b''

    return content


def check_content_type_json(request):
    content = get_content_type_header(request).split()

    if content and CONTENT_TYPE_JSON in content[0].lower():
        return True

    return False


def check_content_type_text(request):
    content = get_content_type_header(request).split()

    if content and CONTENT_TYPE_TEXT in content[0].lower():
        return True

    return False


def check_content_type_form(request):
    content = get_content_type_header(request).split()

    if content and CONTENT_TYPE_FROM_DATA in content[0].lower():
        return True

    return False


def check_content_type_image(request):
    content = get_content_type_header(request).split()

    if content and CONTENT_TYPE_IMAGE in content[0].lower():
        return True

    return False

def check_content_type_video(request):
    content = get_content_type_header(request).split()

    if content and CONTENT_TYPE_VIDEO in content[0].lower():
        return True

    return False


def check_developer_login(request):
    if request:
        addr = request.META.get('REMOTE_ADDR')

        if addr in (IP_SOCKET_LOCAL,):
            return True

    return False


def check_device_otp_valid(request):
    if request:
        if check_developer_login(request):
            pass

        else:
            http_device = request.META.get('HTTP_MNV_DEVICE')

            if not http_device:
                # print('--------- TEST OTP: Khong co Device -----------')
                return False

            otp_cur, otp_pre = get_otp()

            # print('--------- TEST OTP -----------')
            # print('cur: ', otp_cur, ' - pre: ', otp_pre)
            # print(http_device)

            try:
                device_otp = http_device
            except ValueError:
                device_otp = None

            if device_otp not in (otp_cur, otp_pre):
                # logger('------ Không Khớp OTP -----')
                return False

        return True

    return False


def get_encode_header(request):
    if request:
        client_encode = request.META.get('HTTP_MNV_ENCODE')

        if not client_encode:
            return True

        try:
            client_encode = int(client_encode)
        except (TypeError, ValueError):
            client_encode = 1

        if client_encode == 0:
            return False

    return True


def service_encode_from_json(_dict):
    try:
        data = json.dumps(_dict)

        data = data.encode('utf-8')

        data = zlib.compress(data)

        return data
    except Exception as ex:
        # logger('--- Error Encode ---')
        # logger(ex)
        return ''


def service_decode_to_json(_string):

    data = zlib.decompress(_string)

    data = data.decode('utf-8')

    data = json.loads(data)

    # logger('------------Receive Data Decode-------------')
    # logger(json.dumps(data))

    return data
