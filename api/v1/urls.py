from django.conf.urls import include, url
from django.urls import path
from api.v1 import URL_API
from api.v1 import views


app_name = "api_v1"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path('contacts/', views.ContactList.as_view(), name='contacts'),
]