from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import status

from django.http import QueryDict

from core.models import Contact
from core.serializers import ContactSerializer


class ContactList(APIView):
    permission_classes = (IsAuthenticated,)

    """
    List all Contact, or create a new Contact.
    """

    def get(self, request, format=None):

        contacts = Contact.objects.all()
        serializer = ContactSerializer(contacts, many=True)
        
        return Response(serializer.data)

    def post(self, request, format=None):
        phone_number = request.data.get("phone_number", "0")
        if is_valid_phone_number(phone_number):
            # add created_id to identify who can edit or delete this data
            new_request_data = QueryDict('', mutable=True)
            new_request_data.update(request.data)
            new_request_data.update({"created_by":request.user.username})

            serializer = ContactSerializer(data=new_request_data)
            
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response({"error":"Phone number is not valid"}, status=status.HTTP_400_BAD_REQUEST)


# check valid Vietnames phone number (2 types)
def is_valid_phone_number(phone_number):
    # Need to code more
    return True