from django.contrib import admin

# Register your models here.
from core.models import Contact
from core.models import Tag

admin.site.register(Contact)
admin.site.register(Tag)